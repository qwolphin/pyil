#!/usr/bin/env python3
from setuptools import setup

with open("README.md") as f:
    readme = f.read()

setup(
    name="pyil-sh",
    version="0.1",
    description="Ergonomic Python snippets in shell scripts",
    long_description=readme,
    long_description_content_type="text/markdown",
    author="Lumi Akimova",
    author_email="lumi.akimova@gmail.com",
    install_requires=["pyyaml"],
    url="https://gitlab.com/qwolphin/pyil",
    py_modules=["pyil"],
    classifiers=[
        "Intended Audience :: Developers",
        "Development Status :: 4 - Beta",
        "Natural Language :: English",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 2",
    ],
    entry_points={
        "console_scripts": [
            "pyil = pyil:main",
        ]
    },
)
