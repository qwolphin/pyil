#!/usr/bin/env python3
import sys
import os
import argparse
import ast


class Importer:
    def __init__(self, name=""):
        self.name = name
        self.nothing = self.cached = object()

    def __getattr__(self, name):
        if self.name:
            Importer(self.name + "." + name)

        return Importer(name)

    @property
    def _(self):
        if self.cached is self.nothing:
            self.cached = __import__(self.name)

        return self.cached


class DottedDict(dict):
    def __getattr__(self, name):
        return self[name]


class Unpack:
    def __init__(self, data):
        self.data = data

    def __repr__(self):
        return "Unpack({data})".format(data=self.data)


globals_ = {
    "m": Importer(),
    "e": DottedDict(os.environ),
    "Unpack": Unpack,
}


def seq_eval_exec_py2(args, code, seq, is_filter=False):
    glocals = globals_.copy()

    for val in seq:
        try:
            # glocals are needed to avoid breaking generator expressions
            glocals["i"] = val
            exec(code, glocals)
            yield glocals["___return"]
        except Exception as e:
            error = "Evaluation failed {e} (value: {val})\n".format(
                e=repr(e), val=val
            )

            if args.pedantic:
                sys.exit(error[:-1])

            if not args.quiet:
                sys.stderr.write(error)

            if is_filter:
                yield False
            else:
                continue


def seq_eval_exec_py3(args, code, seq, is_filter=False):
    glocals = globals_.copy()
    exec_ = eval("exec")

    for val in seq:
        try:
            # glocals are needed to avoid breaking generator expressions
            glocals["i"] = val
            exec_(code, glocals)
            yield glocals["___return"]
        except Exception as e:
            error = "Evaluation failed {e}, value: {val}\n".format(
                e=repr(e), val=val
            )

            if args.pedantic:
                sys.exit(error[:-1])

            if not args.quiet:
                sys.stderr.write(error)

            if is_filter:
                yield False
            else:
                continue


def seq_eval_eval(args, code, seq, is_filter=False, globals_=globals_, eval=eval):
    glocals = globals_.copy()

    for val in seq:
        try:
            # glocals are needed to avoid breaking generator expressions
            glocals["i"] = val
            yield eval(code, glocals)
        except Exception as e:
            error = "Evaluation failed {e}, value: {val}\n".format(
                e=repr(e), val=val
            )

            if args.pedantic:
                sys.exit(error[:-1])

            if not args.quiet:
                sys.stderr.write(error)

            if is_filter:
                yield False
            else:
                continue


def simple_eval_exec(args, code, locals_):
    try:
        glocals = locals_
        glocals.update(globals_)
        exec(code, glocals)
        return glocals["___return"]
    except Exception as e:
        error = "Evaluation failed: {e}".format(e=repr(e))
        sys.exit(error)


def simple_eval_eval(args, code, locals_):
    try:
        glocals = locals_
        glocals.update(globals_)
        return eval(code, glocals)
    except Exception as e:
        error = "Evaluation failed: {e}".format(e=repr(e))
        sys.exit(error)


def read_values(args, stream=sys.stdin):
    if args.json_in:
        import json

        jl = json.loads
        dd = DottedDict
        return (jl(x, object_hook=dd) for x in stream)

    elif args.yaml_in:
        import yaml

        return yaml.safe_load_all(stream)

    else:
        return (line[:-1] if line[-1] == "\n" else line for line in stream)


def write_values(args, seq):
    stdout = sys.stdout

    if isinstance(seq, str):
        sys.stderr.write("Warning: got a single string instead of values iterable\n")

    if args.json_out:
        import json

        jd = json.dump
        write = stdout.write

        for x in seq:
            jd(x, stdout)
            write("\n")
    elif args.yaml_out:
        import yaml

        yaml.safe_dump_all(seq, stream=stdout)
    else:
        wl = stdout.writelines
        if sys.version_info < (3, 6):
            wl(x.__str__() + "\n" for x in seq)
        else:
            exec("wl(f'{x}\\n' for x in seq)", {"seq": seq, "wl": wl})


def handle_filter(args, code):
    import itertools

    if args.json_out or args.yaml_out:
        sys.stderr.write(
            "Warning: -J/--json-out and -Y/--yaml-out are ignored with -f/--filter\n"
        )

    raw_strings, my_stdin = itertools.tee(sys.stdin)

    if args.yaml_in:
        sys.exit("Using -f/--filter with -y/--yaml-in is not supported (yet)")
    else:
        gen = read_values(args, stream=my_stdin)

    out = args.seq_eval(args, code, gen, is_filter=True)
    sys.stdout.writelines(line for line, val in zip(raw_strings, out) if val)


class SingleLocals(dict):
    """
    The class is emulating mapping for --single
    used to avoid being stuck if stdin unused and empty
    """

    def __init__(self, i_callable):
        self.i_callable = i_callable

    def __missing__(self, key):
        if key == "i":
            self["i"] = value = self.i_callable()
            return value

        raise KeyError(key)


def handle_single(args, code):
    if args.json_in:
        import json

        mk_i = lambda: json.load(sys.stdin, object_hook=DottedDict)
    elif args.yaml_in:
        import yaml

        mk_i = lambda: yaml.safe_load(sys.stdin)
    else:
        mk_i = lambda: sys.stdin.read()

    value = args.simple_eval(args, code, SingleLocals(mk_i))

    if isinstance(value, Unpack):
        write_values(args, value.data)
        return

    if args.json_out:
        import json

        json.dump(value, sys.stdout)
        sys.stdout.write("\n")
    elif args.yaml_out:
        import yaml

        yaml.safe_dump(value, stream=sys.stdout)
    else:
        sys.stdout.write(str(value))


def handle_default(args, code):
    gen = read_values(args)
    out = args.seq_eval(args, code, gen)
    write_values(args, out)


def handle_generator(args, code):
    gen = read_values(args)
    out = args.simple_eval(args, code, {"i": gen})
    write_values(args, out)


description = """
Shortcut for using Python in shell scripts

Provide Python expression in EXPR
(by default it's evaluated once per each line)

Input value is provided in the `i` variable

Example:

$ echo aaa | pyil 'i.upper()'
AAA
"""

parser = argparse.ArgumentParser(
    description=description,
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
parser.set_defaults(main=handle_default)
parser.add_argument("expr", metavar="EXPR", help="expression to evaluate")

input_ = parser.add_mutually_exclusive_group()
input_.add_argument(
    "-j",
    "--json-in",
    action="store_true",
    help="Parse input as JSON",
)
input_.add_argument(
    "-y",
    "--yaml-in",
    action="store_true",
    help="Parse input as YAML",
)

output_ = parser.add_mutually_exclusive_group()
output_.add_argument(
    "-J",
    "--json-out",
    action="store_true",
    help="Serialize output as JSON",
)
output_.add_argument(
    "-Y",
    "--yaml-out",
    action="store_true",
    help="Serialize output as YAML",
)

mode = parser.add_mutually_exclusive_group()
mode.add_argument(
    "-f",
    "--filter",
    dest="main",
    action="store_const",
    const=handle_filter,
    help="Print only lines with corresponding `bool(val) is True`",
)
mode.add_argument(
    "-g",
    "--generator",
    dest="main",
    action="store_const",
    const=handle_generator,
    help="Evaluate expression once, `i` is a generator, iterable is expected",
)
mode.add_argument(
    "-s",
    "--single",
    dest="main",
    action="store_const",
    const=handle_single,
    help="Evaluate expression once, `i` is multiline string or single JSON/YAML value",
)

errors = parser.add_mutually_exclusive_group()
errors.add_argument(
    "-q",
    "--quiet",
    action="store_true",
    help="Ignore errors (do not print to stderr)",
)
errors.add_argument(
    "-p",
    "--pedantic",
    action="store_true",
    help="Exit with non-zero code on first Exception",
)


def main():
    args = parser.parse_args()

    if args.yaml_in or args.yaml_out:
        try:
            import yaml
        except ModuleNotFoundError:
            sys.exit(
                "Module 'yaml' not found. It's required for -y/--yaml-in and -Y/--yaml-out"
            )

    code = ast.parse(args.expr, filename="<pyil expr>", mode="exec")

    try:
        last = code.body.pop()
    except IndexError:
        sys.exit("The code snippet is empty")

    if not isinstance(last, ast.Expr):
        sys.exit(
            "The code snippet should end with an expression (e.g. not with variable assignment)"
        )

    if code.body:
        code.body.append(
            ast.Assign(
                targets=[ast.Name(id="___return", ctx=ast.Store())],
                value=last.value,
            )
        )
        mode = "exec"
        args.simple_eval = simple_eval_exec

        if sys.version_info[0] < 3:
            args.seq_eval = seq_eval_exec_py2
        else:
            args.seq_eval = seq_eval_exec_py3
    else:
        code = ast.Expression(body=last.value)
        mode = "eval"
        args.seq_eval = seq_eval_eval
        args.simple_eval = simple_eval_eval

    code = ast.fix_missing_locations(code)
    code = compile(code, "<pyil code>", mode=mode)
    args.main(args, code)


if __name__ == "__main__":
    main()
