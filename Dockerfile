FROM python:3.11-slim

RUN apt update && apt install -y shelltestrunner && rm -rf /var/lib/apt/lists/*

RUN pip3 install --no-cache twine setuptools wheel pyyaml
